/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pos;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Shibbir
 */
public class show_expenditure extends javax.swing.JFrame {

    /**
     * Creates new form show_expenditure
     */
    connectDB cnnt;

    Calendar currentDate = Calendar.getInstance(); //Get the current date
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); //format it as per your requirement
    String dateNow = formatter.format(currentDate.getTime());
    java.util.Date today = null;
    DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
    String[] seta = new String[200];
    String[] setb = new String[200];
    String[] setc = new String[200];
    String date1 = null, date2 = null, cat, sub_cat, initial_table_query = "select * from expenditure";
int flag=0,flag1=0;
    public show_expenditure() {
        cnnt = new connectDB();
        initComponents();

        jComboBox1.setSelectedIndex(-1);


        try {
            today = (Date) f.parse(dateNow);
        } catch (Exception ex) {
            Logger.getLogger(sell_product.class.getName()).log(Level.SEVERE, null, ex);
        }
        jDateChooser2.setDate(today);
        jDateChooser3.setDate(today);

        cnnt.myConnect();
        Statement s1 = cnnt.getStatement();
        Statement s2 = cnnt.getStatement();
        Statement s3 = cnnt.getStatement();
        Statement s4 = cnnt.getStatement();

        ResultSet r1 = cnnt.getResultset();
        ResultSet r2 = cnnt.getResultset();
        ResultSet r3 = cnnt.getResultset();
        ResultSet r4 = cnnt.getResultset();


        int i = 0;
        try {
            r1 = s1.executeQuery("select distinct exp_cat from exp_category");

            while (r1.next()) {
                seta[i] = r1.getString("exp_cat");
//                System.out.println(seta[i]);
                i++;
            }
//            System.out.println("i = " + (i - 1));

        } catch (SQLException ex) {
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
        }

        //System.out.println("new " + set[0]);
        //System.out.println(set[1]);
        for (int j = 0; j < i; j++) {
            jComboBox1.addItem(seta[j]);
            //  jComboBox3.addItem(seta[j]);
        }
        
        i = 0;
        try {
            r2 = s2.executeQuery("select exp_subcat from exp_category where exp_cat='"+seta[0]+"'");

            while (r2.next()) {
                setb[i] = r2.getString("exp_subcat");
//                System.out.println(seta[i]);
                i++;
            }
//            System.out.println("i = " + (i - 1));

        } catch (SQLException ex) {
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
        }
        jComboBox2.removeAllItems();
        //System.out.println("new " + set[0]);
        //System.out.println(set[1]);
        for (int j = 0; j < i; j++) {
            jComboBox2.addItem(setb[j]);
            //  jComboBox3.addItem(seta[j]);
        }
        
        seta = null;
        System.out.println("lhhh"+today.toString());
          String tonight = ((JTextField) jDateChooser2.getDateEditor().getUiComponent()).getText();
        Table("select exp_cat,exp_subcat,receiver,description,amount,exp_date from expenditure join exp_category using(exp_id) where exp_date = '" + tonight + "'");
    }

    public void Table(String query) {
        try {
            int sum=0;
            cnnt.myConnect();
            Statement stat;
            stat = cnnt.getStatement();
            ResultSet res = cnnt.getResultset();

            res = stat.executeQuery(query);

            DefaultTableModel dtm = new DefaultTableModel();

            dtm.setColumnIdentifiers(new String[]{"Category", "Sub-Category", "Receiver", "Description", "Amount", "Date"});

            int size = 0;
            while (res.next()) {
                size++;
            }

            dtm.setRowCount(size);

            ResultSet r1 = cnnt.getResultset();
            Statement s1 = cnnt.getStatement();

            r1 = s1.executeQuery(query);

            int i = 0;
            while (r1.next()) {
                int amount=r1.getInt("amount");
                dtm.setValueAt(r1.getString("exp_cat"), i, 0);
                dtm.setValueAt(r1.getString("exp_subcat"), i, 1);
                dtm.setValueAt(r1.getString("receiver"), i, 2);
                dtm.setValueAt(r1.getString("description"), i, 3);
                dtm.setValueAt(amount, i, 4);
                dtm.setValueAt(r1.getString("exp_date"), i, 5);
                sum+=amount;
                i++;

            }
            jTable1.setModel(dtm);
jLabel5.setText(""+sum);
        } catch (Exception e) {
//            System.out.println(e.toString());
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        jComboBox2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox2ItemStateChanged(evt);
            }
        });
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        jLabel1.setText("Category");

        jLabel2.setText("Sub-Category");

        jDateChooser2.setDateFormatString("yyyy-MM-dd");
        jDateChooser2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDateChooser2changeDate(evt);
            }
        });

        jDateChooser3.setDateFormatString("yyyy-MM-dd");
        jDateChooser3.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDateChooser3changeDate(evt);
            }
        });

        jLabel3.setText("To");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Category", "Sub-Category", "Receiver", "Description", "Amount", "Date"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel4.setText("Total");

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N

        jLabel6.setText("From");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(170, 170, 170)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(259, 259, 259)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(308, 308, 308)
                        .addComponent(jLabel4)
                        .addGap(41, 41, 41)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 747, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateChooser3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(26, 26, 26))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jDateChooser2changeDate(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDateChooser2changeDate
        // TODO add your handling code here:
        if ("date".equals(evt.getPropertyName())) {
            date1 = ((JTextField) jDateChooser2.getDateEditor().getUiComponent()).getText();
        }
        System.out.println(date1);
                String st = "select exp_cat,exp_subcat,receiver,description,amount,exp_date from expenditure join exp_category using(exp_id) where exp_date between '" + date1 + "' and '" + date2 + "'";
        Table(st);
    }//GEN-LAST:event_jDateChooser2changeDate

    private void jDateChooser3changeDate(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDateChooser3changeDate
        // TODO add your handling code here:
        if ("date".equals(evt.getPropertyName())) {
            date2 = ((JTextField) jDateChooser3.getDateEditor().getUiComponent()).getText();

        }
        String st = "select exp_cat,exp_subcat,receiver,description,amount,exp_date from expenditure join exp_category using(exp_id) where exp_date between '" + date1 + "' and '" + date2 + "'";
        Table(st);
        System.out.println(date2);
        
    }//GEN-LAST:event_jDateChooser3changeDate

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        // TODO add your handling code here:
        if(flag!=0){
           
        connectDB cn = new connectDB();
        cn.myConnect();

        cat = null;
        //setc=null;
        Statement s3 = cn.getStatement();
        ResultSet r3 = cn.getResultset();
        ResultSet r6 = cn.getResultset();
        cat = jComboBox1.getSelectedItem().toString();
       System.out.println("Combo box sel :" + cat);
        int k = 0;
       //setc=null;
        try {
            r3 = s3.executeQuery("select exp_subcat from exp_category where exp_cat like '" + cat + "'");
            r6 = r3;
            while (r6.next()) {
                setc[k] = r6.getString("exp_subcat");
                System.out.println("hi " +k);
                k++;
            }
            //System.out.println("i = " +(i-1));

        } catch (SQLException ex) {
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
        }
      // jComboBox2.removeAllItems();

        //System.out.println("new " + set[0]);
        //System.out.println(set[1]);
       //  jComboBox2.setSelectedIndex(-1);
        flag1=0;
        jComboBox2.removeAllItems();
        flag1=1;
            System.out.println("sdvbjfh");
        for (int j = 0; j < k; j++) {
            System.out.println(setc[j]);
            jComboBox2.addItem(setc[j]);
            System.out.println(setc[j]);
        }
        flag++;
       //setc=null;
        //jComboBox2.revalidate();
        //setc=null;
        //cat=null;*/
        Table("select exp_cat,exp_subcat,receiver,description,amount,exp_date from expenditure join exp_category using(exp_id) where exp_cat='" + cat + "'");
        }
        flag++;
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jComboBox2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox2ItemStateChanged
        // TODO add your handling code here:
       if(flag1>0){
        String st = jComboBox2.getSelectedItem().toString();
        Table("select exp_cat,exp_subcat,receiver,description,amount,exp_date from expenditure join exp_category using(exp_id) where exp_cat='" + cat + "' and exp_subcat='" + st + "'");
    }//GEN-LAST:event_jComboBox2ItemStateChanged
    
    }
       
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(show_expenditure.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(show_expenditure.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(show_expenditure.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(show_expenditure.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
               // FirstPdf n=new FirstPdf("g");
               // n.pdfmain();
                new show_expenditure().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
