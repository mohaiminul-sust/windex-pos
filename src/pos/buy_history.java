/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pos;

import java.sql.Date;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author USER
 */
public class buy_history extends javax.swing.JFrame {

    Connection conn;
    Statement statement;
    ResultSet res;
    connectDB cdb;
    String date1 = null, date2 = null;
    public buy_history() {
        initComponents();
        setAlwaysOnTop(true);
        setLocation(250, 80);
        Table("Select `product_id`, `product_name`, `quantity`, `company`, `invoice_no`, "
                    + " `warranty`, `buying_price`, `selling_price`, `date`,`category`, "
                    + "`sub_category` from buy_history ");
    }
    
    private void Table(String sql) {
        try {
            cdb = new connectDB();
            cdb.myConnect();

            conn = cdb.getConnection();
            statement = cdb.getStatement();
            res = cdb.getResultset();

            res = statement.executeQuery("Select * from buy_history");

            DefaultTableModel dtm = new DefaultTableModel();

            dtm.setColumnIdentifiers(new String[]{"Product ID", "Product Name", "Quantity", "Company", "Invoice No", "Warranty", "Buying Price", "Selling Price", "Date", "Category", "Sub-Category"});

            int size = 0;
            while (res.next()) {
                size++;
            }

            dtm.setRowCount(size);

            ResultSet r1 = cdb.getResultset();
            Statement s1 = cdb.getStatement();

            r1 = s1.executeQuery(sql);

            int i = 0;
            while (r1.next()) {
                dtm.setValueAt(r1.getString(1), i, 0);
                dtm.setValueAt(r1.getString(2), i, 1);
                dtm.setValueAt(r1.getInt(3), i, 2);
                dtm.setValueAt(r1.getString(4), i, 3);
                dtm.setValueAt(r1.getString(5), i, 4);
                dtm.setValueAt(r1.getInt(6), i, 5);
                dtm.setValueAt(r1.getInt(7), i, 6);
                dtm.setValueAt(r1.getInt(8), i, 7);
                dtm.setValueAt(r1.getDate(9), i, 8);
                dtm.setValueAt(r1.getString(10), i, 9);
                dtm.setValueAt(r1.getString(11), i, 10);
                i++;
            }

            jTable1.setModel(dtm);

        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
    }                                        

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {                                         
//        cdb = new connectDB();
//        cdb.myConnect();
//
//        conn = cdb.getConnection();
//        statement = cdb.getStatement();
//        res = cdb.getResultset();
//        
//        try {
//            String location = "C:\\report_buy_add.jrxml";
//            
//            try {
//                InputStream fs = new FileInputStream(new File(location));
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(record_buy_add.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            InputStream is = this.getClass().getResourceAsStream(location);
//            
//            JasperDesign jd = JRXmlLoader.load(location);
//            String query = "Select `product_id`, `product_name`, `product_quantity`, `product_vendor`,"
//                    + " `warranty_period`, `buying_price`, `selling_price`, `buying_date`,`category_main`, "
//                    + "`category_sub` from (product join buy_rec using(product_id)) join category using(cat_id) ";
////                    + "WHERE buying_date BETWEEN '" + date1 + "' AND '" + date2 + "' ;";
//            
//            JRDesignQuery qur = new JRDesignQuery();
//            qur.setText(query);
//            jd.setQuery(qur);
//            JasperReport jasperReport = JasperCompileManager.compileReport(jd);
//            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, conn);
//
//            JasperViewer.viewReport(jasperPrint, false);
//            
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jDateChooser1.setDateFormatString("yyyy-MM-dd");
        jDateChooser1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDateChooser1changeDate1(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("From");

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("To");

        jDateChooser2.setDateFormatString("yyyy-MM-dd");
        jDateChooser2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDateChooser2changeDate2(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Black Chancery", 0, 36)); // NOI18N
        jLabel1.setText("Buy History");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 23, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 788, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
            .addGroup(layout.createSequentialGroup()
                .addGap(231, 231, 231)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jDateChooser1changeDate1(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDateChooser1changeDate1
        if ("date".equals(evt.getPropertyName())) {
            date1 = ((JTextField) jDateChooser1.getDateEditor().getUiComponent()).getText();
            System.out.println("Date : " + date1);
        }
        
        if(date1 != null && date2 != null) {
            Table("Select `product_id`, `product_name`, `quantity`, `company`, `invoice_no`, "
                    + " `warranty`, `buying_price`, `selling_price`, `date`,`category`, "
                    + "`sub_category` from buy_history WHERE date between '"+date1+"' AND '"+date2+"'");
        }
    }//GEN-LAST:event_jDateChooser1changeDate1

    private void jDateChooser2changeDate2(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDateChooser2changeDate2
        if ("date".equals(evt.getPropertyName())) {
            date2 = ((JTextField) jDateChooser2.getDateEditor().getUiComponent()).getText();
            System.out.println("Date : " + date2);
        }
        
        if(date1 != null && date2 != null) {
            Table("Select `product_id`, `product_name`, `quantity`, `company`, `invoice_no`, "
                    + " `warranty`, `buying_price`, `selling_price`, `date`,`category`, "
                    + "`sub_category` from buy_history WHERE date between '"+date1+"' AND '"+date2+"'");
        }
    }//GEN-LAST:event_jDateChooser2changeDate2

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(buy_history.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(buy_history.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(buy_history.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(buy_history.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new buy_history().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
